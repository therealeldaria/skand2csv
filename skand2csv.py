import pandas as pd
from argparse import ArgumentParser
from pathlib import Path
import sys


class MyParser(ArgumentParser):
    def error(self, message):
        self.print_help()
        sys.exit(2)


parser = MyParser(prog="skand2csv",
                  description="""A Python script for converting Skandiabankens Excel transaction exports into Swedish
                   csv text files. They can be used for example to import into Google Sheets, or finance programs"""
                  )
parser.add_argument("inputFile",
                    type=Path,
                    help="The xlsx Excel file exported from Skandibanken",
                    metavar="InputFile.xlsx"
                    )
parser.add_argument("-o",
                    type=Path,
                    help="Output File, if omitted the name of the Input File will be used, but with extension .csv "
                         "written to the current directory.",
                    metavar="OutputFile"
                    )

args = parser.parse_args()

# Get the file paths and check if they are valid
try:
    input_file = args.inputFile.resolve(strict=True)
except FileNotFoundError:
    print(f"Input file '{args.inputFile}' not found, please check your spelling.")
    sys.exit(66)

if args.o is None:
    output_file = (Path(__file__).absolute().parent / input_file.name).with_suffix('.csv')
else:
    try:
        output_file = args.o.parent.resolve(strict=True)
    except FileNotFoundError:
        print(f"Path not found '{args.o.absolute().parent}' not found, please check your spelling.")
        sys.exit(66)
    else:
        output_file = args.o.resolve()

# convert the file to CSV

cols = [0, 1, 2]
df = pd.read_excel(
     input_file,
     usecols=cols,
     skiprows=3,
     header=0,
     dtype={
         'Bokf. datum': str,
         'Beskrivning': str,
         'Belopp': float
     })
df['Beskrivning'] = (df['Beskrivning'].replace('.{4}-..-.. ', '', regex=True))
df.to_csv(output_file, encoding='utf-8', index=False, sep=';', decimal=',')
